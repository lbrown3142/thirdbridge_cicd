#!/bin/sh

echo "=========== docker run: Start =============="

docker run --name some-postgres -e POSTGRES_PASSWORD=mysecretpassword -d postgres

echo "=========== docker run: Done  =============="
